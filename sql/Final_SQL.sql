CREATE DATABASE ScenicSpots CHARSET=utf8mb4;
use ScenicSpots;

CREATE TABLE `ScenicSpots`.`SCENICSPOT`(
 `ScenicName` VARCHAR(100) NOT NULL,
 `OpeningTime` VARCHAR(100) NOT NULL,
 `Address` VARCHAR(100) NOT NULL,
 PRIMARY KEY (`ScenicName`)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

Select * From SCENICSPOT;

CREATE TABLE `ScenicSpots`.`TRAVELINFO`(
  `Name` VARCHAR(100) NOT NULL,
  `Traffic` VARCHAR(150) NOT NULL,
  `Season` VARCHAR(100) NOT NULL,
  `Tips` VARCHAR(200) NOT NULL,
  `LowPrice` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`Name`)
);

Select * From TRAVELINFO;

CREATE TABLE `ScenicSpots`.`EVALUATION`(
   `Place` VARCHAR(100) NOT NULL,
   `Score` float NOT NULL,
   `Visitor` VARCHAR(100) NOT NULL,
   `Comment` VARCHAR(200) NOT NULL,
   `PictureNum` int NOT NULL,
   PRIMARY KEY (`Place`)
);

Select * From EVALUATION;

-- 查询分数大于4.5的景点
SELECT * FROM EVALUATION WHERE Score>4.5;

-- 查询最高分和对应景点
SELECT Place, Score
FROM EVALUATION
WHERE Score = (SELECT MAX(Score) FROM EVALUATION);

-- 查询可以公交抵达的景点
SELECT Name, Traffic
FROM TRAVELINFO
WHERE Traffic LIKE '%公交%';







